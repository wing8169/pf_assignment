#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

const char spaces[] = "                              ";

typedef struct Staff {
    char id[10];
    char name[20];
    char department[10];
    char ic[20];
    char email[20];    
} Staff;

// HELPER FUNCTION
char* concat(const char *s1, const char *s2);
void printWithSpaces(const char* text);
void clearScreen();
void pauseScreen();
void flushBuffer();
////////////////////////////////////////////

// STAFF DAO
int fetchStaff(Staff staff[]);
Staff* searchStaff(char id[]);
void displayStaff();
int modifyStaff(Staff s);
int addStaff(Staff s);
int deleteStaff(char id[]);
////////////////////////////////////////////

// STAFF HANDLER
void searchStaffHandler();
void displayStaffHandler();
void modifyStaffHandler();
void addStaffHandler();
void deleteStaffHandler();
////////////////////////////////////////////

// MENU HANDLER
char mainMenu();
char staffMenu();
void staffMenuHandler();
////////////////////////////////////////////


// main ...
int main(){
    char* moduleNotSupported = concat(spaces, "Module not supported yet.\n");
    char* invalidArgument = concat(spaces, "Invalid input.\n");

    Staff staff[1000];
    int totalStaff = fetchStaff(staff);
    
    while(true){
        flushBuffer();
        char input = mainMenu();
        flushBuffer();
        switch(input){
            case '0':
                return 0;
            case '1':
                staffMenuHandler();
                break;
            case '2':
                printf("%s", moduleNotSupported);
                pauseScreen();
                break;
            case '3':
                printf("%s", moduleNotSupported);
                pauseScreen();
                break;
            case '4':
                printf("%s", moduleNotSupported);
                pauseScreen();
                break;
            default:
                printf("%s", invalidArgument);
                pauseScreen();
                break;
        }
    }

    return 0;
}

// flushBuffer purges all pending input stream
void flushBuffer(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        fpurge(stdin);
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        fflush(stdin);
    #endif
}

// clearScreen is a helper function that clears the CLI screen
void clearScreen(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

// pauseScreen pauses the screen, added compatibility for other OS
void pauseScreen(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system( "read -n 1 -s -p \"Press any key to continue...\"" );
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("pause");
    #endif
}

// modifyStaff modifies existing staff, return 0 if success, 1 if file I/O fails, -1 if record not found
int modifyStaff(Staff s){
    FILE* oriFilePtr;
    FILE* tmpFilePtr;
    char oriFileName[] = "data.bin";
    char tmpFileName[] = "tmp.bin";
    
    if((oriFilePtr = fopen(oriFileName, "rb")) == NULL) {
        return 1;
    }
    if((tmpFilePtr = fopen(tmpFileName, "wb")) == NULL) {
        return 1;
    }

    bool foundRecord = false;

    Staff staff;
    while(fread(&staff, sizeof(Staff), 1, oriFilePtr)){
        if(strcmp(staff.id, s.id) != 0) fwrite(&staff, sizeof(Staff), 1, tmpFilePtr);
        else {
            foundRecord = true;
            if (strcmp(s.name, "0") != 0) strcpy(staff.name, s.name);
            if (strcmp(s.department, "0") != 0) strcpy(staff.department, s.department);
            if (strcmp(s.ic, "0") != 0) strcpy(staff.ic, s.ic);
            if (strcmp(s.email, "0") != 0) strcpy(staff.email, s.email);
            fwrite(&staff, sizeof(Staff), 1, tmpFilePtr);
        }
    }

    fclose(oriFilePtr);
    fclose(tmpFilePtr);
    
    remove(oriFileName);
    rename(tmpFileName, oriFileName);
    
    if(!foundRecord) {
        return -1;
    }

    return 0;
}

// addStaff add new staff to binary file IF DOES NOT EXIST, return 0 if success, 1 if file I/O fails, -1 if record already exists
int addStaff(Staff s){
    Staff* staff = searchStaff(s.id);
    if(staff != NULL) {
        return -1;
    }

    FILE* fptr;
    char fileName[] = "data.bin";
    if((fptr = fopen(fileName, "r+b")) == NULL) {
        return 1;
    }

    fseek(fptr, 0, SEEK_END);
    fwrite(&s, sizeof(Staff), 1, fptr);

    fclose(fptr);

    return 0;
}

// fetchStaff fetches all staff from binary file and return total staff
int fetchStaff(Staff staff[]){
    FILE* fptr;
    char fileName[] = "data.bin";
    if((fptr = fopen(fileName, "rb")) == NULL) {
        printf("unable to open binary file. Creating a new binary file\n");
        fclose(fptr);
        fptr = fopen(fileName, "wb");
        fclose(fptr);
        fptr = fopen(fileName, "rb");
    }

    Staff s;
    int currentStaffIndex = 0;
    while(fread(&s, sizeof(Staff), 1, fptr)){
        staff[currentStaffIndex++] = s;
    }

    fclose(fptr);

    return currentStaffIndex;
}

// displayStaff displays all the staff in binary file
void displayStaff(){
    Staff staff[1000];
    int totalStaff = fetchStaff(staff);
    
    printf("%10s%20s%20s%20s%20s\n", "ID", "Name", "Department", "IC", "Email");
    for(int i=0; i<totalStaff; i++){
        printf("%10s%20s%20s%20s%20s\n", staff[i].id, staff[i].name, staff[i].department, staff[i].ic, staff[i].email); 
    }
    printf("\n");
}

// searchStaff searches, and returns staff if exist else NULL
Staff* searchStaff(char id[]){
    Staff staff[1000];
    int totalStaff = fetchStaff(staff);
    for(int i=0; i<totalStaff; i++){
        if(strcmp(staff[i].id, id) == 0) {
            Staff* s = malloc(sizeof(Staff));
            memcpy(s, &staff[i], sizeof(Staff));

            return s;
        }
    }
    return NULL;
}

// deleteStaff searches, deletes and returns 0 if success, 1 if file I/O fails, -1 if record not found
int deleteStaff(char id[]){
    FILE* oriFilePtr;
    FILE* tmpFilePtr;
    char oriFileName[] = "data.bin";
    char tmpFileName[] = "tmp.bin";
    
    if((oriFilePtr = fopen(oriFileName, "rb")) == NULL) {
        return 1;
    }
    if((tmpFilePtr = fopen(tmpFileName, "wb")) == NULL) {
        return 1;
    }

    bool foundRecord = false;

    Staff s;
    while(fread(&s, sizeof(Staff), 1, oriFilePtr)){
        if(strcmp(s.id, id) != 0) fwrite(&s, sizeof(Staff), 1, tmpFilePtr);
        else foundRecord = true;
    }

    fclose(oriFilePtr);
    fclose(tmpFilePtr);
    
    remove(oriFileName);
    rename(tmpFileName, oriFileName);
    
    if(!foundRecord) {
        return -1;
    }

    return 0;
}

// concat is a helper function for string concatenation
char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

// printWithSpaces is a helper function that prints text by prepending spaces
void printWithSpaces(const char* text) {
    char* tmp = concat(spaces, text);
    printf("%s", tmp);
}

// mainMenu is the menu where user chooses mondule
char mainMenu(){
    clearScreen();
    printf("\n\n\n\n\n\n");
    char* tmp = concat(spaces, "Welcome to PFFF Portal\n");
    printf("%s", tmp);
    tmp = concat(spaces, "1. Staff Database Management Module\n");
    printf("%s", tmp);
    tmp = concat(spaces, "2. User Database Management Module\n");
    printf("%s", tmp);
    tmp = concat(spaces, "3. Booking Database Management Module\n");
    printf("%s", tmp);
    tmp = concat(spaces, "4. Facility Usage Dataabase Management Module\n");
    printf("%s", tmp);
    printf("\n");
    tmp = concat(spaces, "Input(0 to exit): ");
    printf("%s", tmp);
    char userInput;
    scanf("%c", &userInput);
    return userInput;
}

// staffMenu is the menu where user chooses operation to staff database
char staffMenu(){
    clearScreen();
    printf("\n\n\n\n\n\n");
    printWithSpaces("Staff Database Management Modue\n");
    printWithSpaces("1. Search staff by ID\n");
    printWithSpaces("2. Display all staff\n");
    printWithSpaces("3. Modify staff by ID\n");
    printWithSpaces("4. Add staff\n");
    printWithSpaces("5. Delete staff\n");
    printf("\n");
    printWithSpaces("Input(0 to back): ");
    char userInput;
    scanf("%c", &userInput);
    return userInput;
}

// staffMenu handler is the menu where program decides which handler to call for staff database
void staffMenuHandler(){
    char* operationNotSupported = concat(spaces, "Operation not supported yet.\n");
    char* invalidArgument = concat(spaces, "Invalid input.\n");

    while(true){
        flushBuffer();
        char input = staffMenu();
        flushBuffer();
        switch(input){
            case '0':
                return;
            case '1':
                searchStaffHandler();
                pauseScreen();
                break;
            case '2':
                displayStaffHandler();
                pauseScreen();
                break;
            case '3':
                modifyStaffHandler();
                pauseScreen();
                break;
            case '4':
                addStaffHandler();
                pauseScreen();
                break;
            case '5':
                deleteStaffHandler();
                pauseScreen();
                break;
            default:
                printf("%s", invalidArgument);
                pauseScreen();
                break;
        }
    }
}

// displayStaffHandler is the handler to display all staff
void displayStaffHandler(){
    clearScreen();
    printf("\n\n\n\n\n\n");
    printf("%50s\n\n", "Staff List");
    displayStaff();
}

// searchStaffHandler is the handler to search staff by ID
void searchStaffHandler(){
    flushBuffer();
    clearScreen();
    printf("\n\n\n\n\n\n");
    printWithSpaces("Search Staff By ID\n\n");
    printWithSpaces("Enter Staff ID: ");
    
    char userInput[255];
    scanf("%s", userInput);
    
    Staff* s = searchStaff(userInput);
    
    if (s == NULL) {
        printf("\n");
        printWithSpaces("Staff not found, please try again with another ID.\n");
        return;
    }

    printf("\n%50s\n", "Staff Record");
    printf("%10s%20s%20s%20s%20s\n", "ID", "Name", "Department", "IC", "Email");
    printf("%10s%20s%20s%20s%20s\n\n", (*s).id, (*s).name, (*s).department, (*s).ic, (*s).email); 
}

// deleteStaffHandler is the handler to delete staff by ID
void deleteStaffHandler(){
    flushBuffer();
    clearScreen();
    printf("\n\n\n\n\n\n");
    printWithSpaces("Delete Staff By ID\n\n");
    printWithSpaces("Enter Staff ID: ");
    
    char userInput[255];
    scanf("%s", userInput);
    
    int result = deleteStaff(userInput);
    
    if (result == 1) {
        printf("\n");
        printWithSpaces("File I/O failed, please try again.\n");
        return;
    }

    if (result == -1) {
        printf("\n");
        printWithSpaces("Staff not found, please try again with another ID.\n");
        return;
    }

    printf("\n");
    printWithSpaces("Staff record is successfully deleted. ");
    printf("Staff ID: %s\n", userInput);
}

// addStaffHandler is the handler to add new staff if doesn't exist
void addStaffHandler(){
    flushBuffer();
    clearScreen();
    printf("\n\n\n\n\n\n");
    printWithSpaces("Add Staff By ID\n\n");

    Staff s;

    printWithSpaces("Enter Staff ID: ");
    fgets(s.id, sizeof(s.id), stdin);
    s.id[strcspn(s.id, "\n")] = 0;
    printWithSpaces("Enter Staff Name: ");
    fgets(s.name, sizeof(s.name), stdin);
    s.name[strcspn(s.name, "\n")] = 0;
    printWithSpaces("Enter Staff Department: ");
    fgets(s.department, sizeof(s.department), stdin);
    s.department[strcspn(s.department, "\n")] = 0;
    printWithSpaces("Enter Staff IC: ");
    fgets(s.ic, sizeof(s.ic), stdin);
    s.ic[strcspn(s.ic, "\n")] = 0;
    printWithSpaces("Enter Staff Email: ");
    fgets(s.email, sizeof(s.email), stdin);
    s.email[strcspn(s.email, "\n")] = 0;
    
    int result = addStaff(s);
    
    if (result == 1) {
        printf("\n");
        printWithSpaces("File I/O failed, please try again.\n");
        return;
    }

    if (result == -1) {
        printf("\n");
        printWithSpaces("Staff ID already exist, please try again with another ID.\n");
        return;
    }

    printf("\n");
    printWithSpaces("Staff record is successfully added. ");
    printf("Staff ID: %s\n", s.id);
}

// modifyStaffHandler is the handler to modify staff by ID
void modifyStaffHandler(){
    flushBuffer();
    clearScreen();
    printf("\n\n\n\n\n\n");
    printWithSpaces("Modify Staff By ID\n\n");

    Staff s;

    printWithSpaces("Enter Staff ID: ");
    fgets(s.id, sizeof(s.id), stdin);
    s.id[strcspn(s.id, "\n")] = 0;
    printWithSpaces("Enter Staff Name (Enter 0 to skip update): ");
    fgets(s.name, sizeof(s.name), stdin);
    s.name[strcspn(s.name, "\n")] = 0;
    printWithSpaces("Enter Staff Department (Enter 0 to skip update): ");
    fgets(s.department, sizeof(s.department), stdin);
    s.department[strcspn(s.department, "\n")] = 0;
    printWithSpaces("Enter Staff IC (Enter 0 to skip update): ");
    fgets(s.ic, sizeof(s.ic), stdin);
    s.ic[strcspn(s.ic, "\n")] = 0;
    printWithSpaces("Enter Staff Email (Enter 0 to skip update): ");
    fgets(s.email, sizeof(s.email), stdin);
    s.email[strcspn(s.email, "\n")] = 0;
    
    int result = modifyStaff(s);
    
    if (result == 1) {
        printf("\n");
        printWithSpaces("File I/O failed, please try again.\n");
        return;
    }

    if (result == -1) {
        printf("\n");
        printWithSpaces("Staff ID does not exist, please try again with another ID.\n");
        return;
    }

    printf("\n");
    printWithSpaces("Staff record is successfully updated. ");
    printf("Staff ID: %s\n", s.id);
}